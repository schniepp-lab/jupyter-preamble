# %matplotlib inline				# Magic IPython function: https://stackoverflow.com/questions/43027980/purpose-of-matplotlib-inline

from	__future__		import division	# this gets rid of the annoying integer division that's default in python.
#from	numpy			import *	# numerical package. Needs to be done before sympy: "diff" of the two packages is incompatible. Now used as 'np'
import	numpy			as np		# numerical package. 
from	sympy			import *	# symbolic evaluation
from	sympy.physics.units	import *	# units
from	matplotlib		import *	# cool plotting
import	matplotlib.pyplot	as plt		# abbreviation (standard in many examples)
#from   physics			import *	# Alternative unit system. Not currently used.
#from	fractions		import *	# not currently used

init_printing()					# This makes output pretty (uses LaTeX if installed)

# Defining a few units that were missing

mw = milliwatt = milliwatts = Quantity('milliwatt',abbrev='mW')
milliwatt.set_global_relative_scale_factor(milli,watt)

kW = kilowatt = kilowatts = Quantity('kilowatt',abbrev='kW')
kilowatt.set_global_relative_scale_factor(kilo,watt)

mrad = milliradian = milliradians = Quantity('milliradian',abbrev='mrad')
milliradian.set_global_relative_scale_factor(milli,radian)

gal = gallon = gallons = Quantity('gallon', abbrev='gal')
gallon.set_global_relative_scale_factor(convert_to(231*inch**3,meter**3)/meter**3,meter**3)

mN = millinewton = millinewtons = Quantity('millinewtown',abbrev='mN')
millinewton.set_global_relative_scale_factor(milli,newton)

uN = micronewton = micronewtons = Quantity('micronewtown',abbrev='uN')
micronewton.set_global_relative_scale_factor(micro,newton)

nN = nanonewton = nanonewtons = Quantity('nanonewtown',abbrev='nN')
nanonewton.set_global_relative_scale_factor(nano,newton)

pN = piconewton = piconewtons = Quantity('piconewtown',abbrev='pN')
piconewton.set_global_relative_scale_factor(pico,newton)

aN = attonewton = attonewtons = Quantity('attonewtown',abbrev='aN')
attonewton.set_global_relative_scale_factor(atto,newton)

zN = zeptonewton = zeptonewtons = Quantity('zeptonewtown',abbrev='zN')
zeptonewton.set_global_relative_scale_factor(zepto,newton)

mJ = millijoule = millijoules = Quantity('millijoules',abbrev='mJ')
millijoule.set_global_relative_scale_factor(milli,joule)

uJ = microjoule = microjoules = Quantity('microjoules',abbrev='uJ')
microjoule.set_global_relative_scale_factor(micro,joule)

nJ = nanojoule = nanojoules = Quantity('nanojoules',abbrev='nJ')
nanojoule.set_global_relative_scale_factor(nano,joule)

pJ = picojoule = picojoules = Quantity('picojoules',abbrev='pJ')
picojoule.set_global_relative_scale_factor(pico,joule)

aJ = attojoule = attojoules = Quantity('attonjoules',abbrev='aJ')
attojoule.set_global_relative_scale_factor(atto,joule)

zJ = zeptojoule = zeptojoules = Quantity('zeptojoules',abbrev='zJ')
zeptojoule.set_global_relative_scale_factor(zepto,joule)

MPa = megapascal = megapascals = Quantity('megapascal',abbrev='MPa')
megapascal.set_global_relative_scale_factor(Mega,pascal)

GPa = gigapascal = gigapascals = Quantity('gigapascal',abbrev='GPa')
gigapascal.set_global_relative_scale_factor(Giga,pascal)
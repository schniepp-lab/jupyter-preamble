This is a publicly shared jupyter preamble used by the **nano & biomaterials lab**, to be loaded before executing any jupyter notebooks used and produced by the group. It includes common definitions (units used for our research) and imports (sympy, numpy, matplotlib, units). The idea is that all the commonly used includes and definitions are preloaded, so we don't have to repeat them for every single notebook. All users sharing the same preamble can all share notebooks, and they will work.

On my Ubuntu system, this file goes here:
~/ipython/profile_default/startup/
